import Vue from 'vue';
import { MLInstaller, MLCreate, MLanguage } from 'vue-multilanguage';

Vue.use(MLInstaller);

export default new MLCreate({
  initial: 'english',
  save: process.env.NODE_ENV === 'production',
  languages: [
    new MLanguage('english').create({
      _about: 'About me',
      about: 'I am a person that will take care of your website user experience and interface. If you are using Vue.js stack I can develop the frontend of it in high stands of code quality. I am used to the AWS cloud stack in order to deliver backend and frontend solutions. My goal is to provide you not with a basic website, but to help you evaluate and define your web presence. I am a fluent english speaker and ongoing french student. I hold Bachelors of Computer Engineering and would really enjoy helping you =)',
      ttme: 'Talk to me'
    }),
    new MLanguage('portuguese').create({
      _about: 'Sobre mim',
      about: 'Cara legal pkct',
      ttme: 'Fale comigo'
    }),
    new MLanguage('français').create({
      _about: 'A propos de moi',
      about: 'Un bon gar',
      ttme: 'Contactez moi'
    })
  ]
});
