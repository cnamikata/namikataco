import Vue from 'vue';
import App from './App.vue';
import VueScrollTo from 'vue-scrollto';
// You can change this import to `import router from './starterRouter'` to quickly start development from a blank layout.
import router from './router';
import NowUiKit from './plugins/now-ui-kit';
import './plugins/ml'

import Amplify, * as AmplifyModules from 'aws-amplify'
import { AmplifyPlugin } from 'aws-amplify-vue'
import aws_exports from './aws-exports'

Amplify.configure(aws_exports)

Vue.use(AmplifyPlugin, AmplifyModules)

Vue.config.productionTip = false;

Vue.use(NowUiKit);
Vue.use(VueScrollTo);


new Vue({
  router,
  render: h => h(App)
}).$mount('#app');
